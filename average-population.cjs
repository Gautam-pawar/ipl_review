//Find the average population of a country for each sub region for each region 

const countries = require("./countries.json");

const Avg_population_per_subregion_per_region = {};


for (let index = 0; index < countries.length; index++) {


    let Region = countries[index]["region"];



    if (Avg_population_per_subregion_per_region[Region]) {

    } else {

        let obj = {};


        Avg_population_per_subregion_per_region[Region] = obj;
    }
}


for (let key in Avg_population_per_subregion_per_region) {

    let obj = {};

    for (let index = 0; index < countries.length; index++) {

        let Region = countries[index]["region"];

        let Subregion = countries[index]["subregion"];

        let Population = countries[index]["population"]


        if (key === Region) {


            if (obj[Subregion]) {

                let temp = obj[Subregion];

                temp[0] += Population;

                temp[1] += 1;


            } else {
                let array = new Array(2);

                array[0] = Population;

                array[1] = 1;

                obj[Subregion] = array;

            }


        }

        Avg_population_per_subregion_per_region[key] = obj;

    }


}



for (let key in Avg_population_per_subregion_per_region) {
    const vale = Avg_population_per_subregion_per_region[key]
    for (let key2 in vale) {
        let val1 = vale[key2]
        let avg = val1[0] / val1[1];

        vale[key2] = parseInt(avg.toFixed(2));
    }
}

console.log(Avg_population_per_subregion_per_region);