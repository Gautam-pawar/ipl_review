// Find the names of countries whose population is less than 10000

const countries = require("./countries.json");

const less_populated_countries = [] ;

for(let index = 0 ; index < countries.length ; index++){

    let population = countries[index]["population"] ;

    let country = countries[index]["name"]["official"];

    if (population < 10000){

        less_populated_countries.push(country);
    }
}

console.log(less_populated_countries);