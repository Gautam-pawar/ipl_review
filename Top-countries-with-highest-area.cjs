
//Find the top 5 countries with the highest area 

const countries = require("./countries.json");

const country_areas = {};


for (let index = 0; index < countries.length; index++) {


    let Area = countries[index]["area"];

    let Country_name = countries[index]["name"]["official"];

    country_areas[Country_name] = Area;

}


let sorted_array = [];

for (let key in country_areas) {

    sorted_array.push([key, country_areas[key]]);
}

sorted_array.sort(function (a, b) {

    return b[1] - a[1];

});

for (let sortedindex = 0; sortedindex < 5; sortedindex++) {

    console.log(sorted_array[sortedindex]);
}