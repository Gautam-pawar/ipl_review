
//Find the number of countries for each region per sub region

const countries = require("./countries.json");

const total_cout = {};

for (let index = 0; index < countries.length; index++) {

    let Region = countries[index]["region"];

    if (total_cout[Region] === undefined) {

        let emptyobject = {};

        total_cout[Region] = emptyobject;
    }

}



for (let key in total_cout) {

    let obj = {};

    for (let index = 0; index < countries.length; index++) {

        let Subregion = countries[index]["subregion"];

        let Region = countries[index]["region"];

        if (key == Region) {

            if (obj[Subregion]) {

                obj[Subregion] += 1;

            } else {

                obj[Subregion] = 1;
            }

        }

    }
    
    total_cout[key] = obj;

}

console.log(total_cout);