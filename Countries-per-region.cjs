// Find the number of countries for each region

const countries = require("./countries.json");

const total_counties_per_region = {} ; 

for(let index = 0 ; index < countries.length ; index++){

    let region = countries[index]["region"] ;

    if (total_counties_per_region[region]){

        total_counties_per_region[region] += 1 ; 
    }else{

        total_counties_per_region[region] =  1 ; 
    }

}

console.log(total_counties_per_region);